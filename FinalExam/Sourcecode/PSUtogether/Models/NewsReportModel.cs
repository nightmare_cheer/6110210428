using Microsoft.AspNetCore.Identity;
using System;
using System.ComponentModel.DataAnnotations;

namespace NewsReport.Models
{
 public class NewsCategory {
 public int NewsCategoryID { get; set;}
 public string ShortName {get; set;}
 public string FullName { get; set;}
 }
 public class News {
 public int NewsID { get; set; }
 public int NewsCategoryID { get; set; }
 public NewsCategory NewsCat { get; set; }
 [DataType(DataType.Date)]
 public string TutoringDate { get; set; }
 public string TutoringSubj { get; set; }
 public string TutoringName { get; set; }
 public string TutoringLocat { get; set; }
 public string TutoringTime { get; set; }
 public float TutoringMoney { get; set; }

 public string NewsUserId {get; set;}
 public NewsUser postUser {get; set;}
 
 }
 public class NewsUser : IdentityUser{
 public string FirstName { get; set;}
 public string LastName { get; set;}
 }


}